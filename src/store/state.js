const State = () => {
    return {
        view: {
            pgAuth: false,
            pgCowork: false
        },
        nav: {
            items: []
        }
    }
}

export default State
