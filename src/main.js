import '@riotjs/hot-reload'
import { component, install } from 'riot'
import route from 'riot-route'
import "./media/favicon.png"

import store from './store/store.js'

import './style.scss'

import App from './app.riot'

install(function(component) {
    component.states$ = store.states$
    component.actions = store.actions
    return component
})

component(App)(document.getElementById('root'))

route.base('#/')
route.start(true)


