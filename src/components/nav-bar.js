export const nav = {
    'pg-admin': [
        {
            name: 'Cowork',
            href: '#/cowork'
        },
        {
            name: 'Menu A',
            items: [
                {
                    name: 'Item 1',
                },
                {
                    name: 'Item 2'
                },
                {
                    name: 'Item 3'
                }
            ]
        },
        {
            name: 'Menu B',
            items: [
                {
                    name: 'Item 1',
                },
                {
                    name: 'Item 2'
                }
            ]
        },
    ],
    'pg-cowork': [
        {
            name: 'Admin',
            href: '#'
        },
        {
            name: 'Menu A',
            items: [
                {
                    name: 'Item 1',
                },
                {
                    name: 'Item 2'
                },
                {
                    name: 'Item 3'
                }
            ]
        },
        {
            name: 'Menu B',
            items: [
                {
                    name: 'Item 1',
                },
                {
                    name: 'Item 2'
                }
            ]
        },
    ]
}
